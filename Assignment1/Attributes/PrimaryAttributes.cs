﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.Attributes
{
    public class PrimaryAttributes
    {
        /// <summary>
        /// Constructor for the PrimaryAttributes class which will set attributes.
        /// </summary>
        /// <param name="strength">Hero strength attribute.</param>
        /// <param name="dexterity">Hero dexterity attribute.</param>
        /// <param name="intelligence">Hero intelligence attribute.</param>
        public PrimaryAttributes(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public static PrimaryAttributes operator + (PrimaryAttributes self, PrimaryAttributes other)
        {
            return new PrimaryAttributes(
                strength: self.Strength + other.Strength,
                dexterity: self.Dexterity + other.Dexterity,
                intelligence: self.Intelligence + other.Intelligence
            );
        }
    }
}
