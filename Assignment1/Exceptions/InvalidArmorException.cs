﻿using System.Runtime.Serialization;

namespace Assignment1.Exceptions
{
    [Serializable]
    public class InvalidArmorException : Exception
    {
        /// <summary>
        /// Throw exception when equipped armor is invalid.
        /// </summary>
        public InvalidArmorException()
        {
        }

        /// <summary>
        /// Throws exception when equipped armor is invalid.
        /// </summary>
        /// <param name="message">Exception reasoning message.</param>
        public InvalidArmorException(string? message) : base(message)
        {
        }
    }
}