﻿using System.Runtime.Serialization;

namespace Assignment1.Exceptions
{
    [Serializable]
    public class InvalidWeaponException : Exception
    {
        /// <summary>
        /// Throw exception when equipped weapon is invalid.
        /// </summary>
        public InvalidWeaponException()
        {
        }

        /// <summary>
        /// Throws exception when equipped weapon is invalid.
        /// </summary>
        /// <param name="message">Exception reasoning message.</param>
        public InvalidWeaponException(string? message) : base(message)
        {
        }
    }
}