﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.Heroes
{
    public static class CreateHero
    {

        /// <summary>
        /// Hero factory used for unit testing.
        /// </summary>
        /// <param name="heroName">The hero chosen.</param>
        /// <returns>New hero made.</returns>
        public static Hero createHeroes(HeroName heroName)
        {
            return heroName switch
            {
                HeroName.Warrior => new Warrior("Jan"),
                HeroName.Ranger => new Ranger("Are"),
                HeroName.Rogue => new Rogue("Ole"),
                HeroName.Mage => new Mage("Per"),
            };
        }
    }
}
