﻿using Assignment1.Attributes;
using Assignment1.Exceptions;
using Assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Assignment1.Items.Armor;
using static Assignment1.Items.Weapon;

namespace Assignment1.Heroes
{
    /// <summary>
    /// The hero base class, which have the the characthers Mage, Rogue, Warrior
    /// and Ranger as its children.
    /// </summary>
    public abstract class Hero
    {
        public int Level { get; set; }

        StringBuilder Stats = new StringBuilder();

        public string Name { get; set; }

        public HeroName HeroType { get; set; }

        public PrimaryAttributes PrimaryAttributes { get; set; }

        public PrimaryAttributes ArmorAttributes { get; set; }

        public List<Weapons> AllowedWeapons = new List<Weapons>();

        public List<Armors> AllowedArmor = new List<Armors>();

        public Dictionary<Slots, Item> Equipment { get; set; } = new Dictionary<Slots, Item>();

        public double BaseDamage { get; set; }

        /// <summary>
        /// Equip weapon in the Equipment dictionary.
        /// </summary>
        /// <param name="weapon">Weapon and its values which are added to the Equipment dictionary.</param>
        /// <returns>Success string</returns>
        /// <exception cref="InvalidWeaponException">When the weapon is invalid for the spesific hero, or the heros level is too low.</exception>
        public string WeaponEquip(Weapon weapon)
        {
            if (!AllowedWeapons.Contains(weapon.Type))
            {
                throw new InvalidWeaponException("Invalid weapon for this hero.");
            }
            else if (weapon.EquipItemLevelReq > Level)
            {
                throw new InvalidWeaponException("Invalid weapon at this hero level.");
            }
            Equipment[Slots.Weapon] = weapon;
            Console.WriteLine("New weapon equipped!\n");
            return "New weapon equipped!";
        }

        /// <summary>
        /// Equip armor in the Equipment dictionary.
        /// </summary>
        /// <param name="armor">Armor and its values which is added to the Equipment dictionary.</param>
        /// <param name="slot">What Equipment slot the armor is being placed in inside the dictionary.</param>
        /// <returns>Success string</returns>
        /// <exception cref="InvalidArmorException">When the armor is either invalid for the spesific hero, or the heros level is too low.</exception>
        public string ArmorEquip(Armor armor, Slots slot)
        {
            if (!AllowedArmor.Contains(armor.Type))
            {
                throw new InvalidArmorException("Invalid armor for this hero.");
            }
            else if (armor.EquipItemLevelReq > Level)
            {
                throw new InvalidArmorException("Invalid armor at this hero level.");
            }
            Equipment[slot] = armor;
            Console.WriteLine("New armour equipped!\n");
            return "New armour equipped!";
        }

        /// <summary>
        /// Displays the heros name, level, attributes and damage.
        /// </summary>
        public void StatsDisplay()
        {
            Stats.AppendLine($"The hero is named {Name}");
            Stats.AppendLine($"{Name} is level {Level}");
            Stats.AppendLine($"Strength: {TotalAttributeCalc().Strength}");
            Stats.AppendLine($"Dexterity: {TotalAttributeCalc().Dexterity}");
            Stats.AppendLine($"Intelligence: {TotalAttributeCalc().Intelligence}");
            Stats.AppendLine($"Damage: {CalcDamage()}");
            Console.WriteLine(Stats);
        }

        /// <summary>
        /// Calculates the total attributes for a hero. Armor + base attributes.
        /// </summary>
        /// <returns>Finished calculated attributes</returns>
        public PrimaryAttributes TotalAttributeCalc()
        {
            PrimaryAttributes result = new PrimaryAttributes(0, 0, 0);
            foreach(var item in Equipment)
            {
                if(item.Value.GetType != null && item.Value.GetType() == typeof(Armor))
                {
                    result += ((Armor)item.Value).PrimaryAttribute;
                }
            }

            return PrimaryAttributes+result;
        }

        /// <summary>
        /// Calculates amount of damage, either without weapons and armor, or with one or both.
        /// </summary>
        /// <returns>The heroes damage</returns>
        public abstract double CalcDamage();

        /// <summary>
        /// Levlels up the hero and add the correct attributes to the already existing onces.
        /// </summary>
        public abstract void LevelUp(); 
    }
}
