﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.Heroes
{
    /// <summary>
    /// Enum with the hero names
    /// </summary>
    public enum HeroName
    {
        Mage,
        Ranger,
        Rogue,
        Warrior,
    }
}
