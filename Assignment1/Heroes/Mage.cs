﻿using Assignment1.Attributes;
using Assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Assignment1.Items.Armor;
using static Assignment1.Items.Weapon;
using static System.Runtime.Intrinsics.Arm.AdvSimd;

namespace Assignment1.Heroes
{
    /// <summary>
    /// Mage is a child class inheriting from the base class Hero. It adds the allowed character weapon to a list, and sets the starting level to default 1.
    /// </summary>
    public class Mage : Hero
    {
        List<Weapons> WeaponsAllowed = new List<Weapons>()
        {
            Weapons.Wand,
            Weapons.Staff,
        };
        List<Armors> ArmorsAllowed = new List<Armors>()
        {
            Armors.Cloth,
        };

        public int LEVEL = 1;

        /// <summary>
        /// Mage hero constructor which initialize Mage with correct level, name, attributes and allowed armor and weapons.
        /// </summary>
        public Mage(string name)
        {
            Level = LEVEL;
            Name = name;
            PrimaryAttributes = new(1, 1, 8);

            foreach (var weapon in WeaponsAllowed)
            {
                AllowedWeapons.Add(weapon);
            }

            foreach (var armor in ArmorsAllowed)
            {
                AllowedArmor.Add(armor);
            }

            Console.WriteLine($"{Name} has just came alive! \n");
        }

        public override void LevelUp()
        {
            Console.WriteLine($"Leveling up {Name} \n");
            Level += 1;
            PrimaryAttributes.Strength += 1;
            PrimaryAttributes.Dexterity += 1;
            PrimaryAttributes.Intelligence += 5;
        }

        public override double CalcDamage()
        {
            double addedAttributeArmor = 0;
            foreach (var item in Equipment)
            {
                if (item.Value.GetType != null && item.Value.GetType() == typeof(Armor))
                {
                    addedAttributeArmor += ((Armor)item.Value).PrimaryAttribute.Intelligence;

                }
            }

            Weapon weaponEquipped = null;
            BaseDamage = ((PrimaryAttributes.Intelligence + addedAttributeArmor )/ 100.0) + 1.0;

            if (!Equipment.TryGetValue(Slots.Weapon, out Item weapon))
            {
                return BaseDamage;
            }
            else if ((Weapon)Equipment[Slots.Weapon] != null)
            {
                weaponEquipped = (Weapon)Equipment[Slots.Weapon];
                if (weaponEquipped == null)
                {
                    return BaseDamage;
                }
            }
    
            double weaponDps = weaponEquipped.GetDamagePerSecond();
            return weaponDps * BaseDamage; 
        }
    }
}
