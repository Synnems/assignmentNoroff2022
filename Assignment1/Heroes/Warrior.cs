﻿using Assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Assignment1.Items.Armor;
using static Assignment1.Items.Weapon;

namespace Assignment1.Heroes
{
    /// <summary>
    /// Warrior is a child class inheriting from the base class Hero. It adds the allowed character weapon to a list, and sets the starting level to default 1.
    /// </summary>
    public class Warrior : Hero
    {
        
        public int LEVEL = 1;
        List<Weapons> WeaponsAllowed = new List<Weapons>()
        {
            Weapons.Axe,
            Weapons.Sword,
            Weapons.Hammer,
        };
        List<Armors> ArmorsAllowed = new List<Armors>()
        {
            Armors.Plate,
            Armors.Mail,
        };

        /// <summary>
        /// Warrior hero contructor which initialize Warrior with correct level, name, attributes and allowed armor and weapons.
        /// </summary>
        public Warrior(string name)
        {
            Level = LEVEL;
            Name = name;
            PrimaryAttributes = new(5, 2, 1);

            foreach (var weapon in WeaponsAllowed)
            {
                AllowedWeapons.Add(weapon);
            }

            foreach (var armor in ArmorsAllowed)
            {
                AllowedArmor.Add(armor);
            }

            Console.WriteLine($"{Name} has just came alive! \n");
        }

        public override double CalcDamage()
        {
            Weapon weaponEquipped = null;
            double addedAttributeArmor = 0;
            foreach (var item in Equipment)
            {
                if (item.Value.GetType != null && item.Value.GetType() == typeof(Armor))
                {
                    addedAttributeArmor += ((Armor)item.Value).PrimaryAttribute.Strength;
                }
            }
            BaseDamage = ((PrimaryAttributes.Strength + addedAttributeArmor) / 100.0) + 1.0;

            if (!Equipment.TryGetValue(Slots.Weapon, out Item weapon))
            {
                return BaseDamage;
            }
            else if ((Weapon)Equipment[Slots.Weapon] != null)
            {
                weaponEquipped = (Weapon)Equipment[Slots.Weapon];
                if (weaponEquipped == null)
                {
                    return BaseDamage;
                }
            }
            double weaponDps = weaponEquipped.GetDamagePerSecond();
            return weaponDps * BaseDamage;
        }

        public override void LevelUp()
        {
            Console.WriteLine($"Leveling up {Name} \n");
            Level += 1;
            PrimaryAttributes.Dexterity += 2;
            PrimaryAttributes.Intelligence += 1;
            PrimaryAttributes.Strength += 3;
        }
    }
}
