﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment1.Attributes;

namespace Assignment1.Items
{
    /// <summary>
    /// Armor is a child class inheriting from the base class Item.
    /// </summary>
    public class Armor : Item
    {

        /// <summary>
        /// Armor constructor used to initalize armors.
        /// </summary>
        /// <param name="name">Name of armor</param>
        /// <param name="equipItemLevelReq">What hero level is required to equip armor.</param>
        /// <param name="itemSlot">What type of item slot the armor belongs in</param>
        /// <param name="primaryAttribute">The attributes the hero will get in addition with the armor piece</param>
        /// <param name="type">What type of armor it is</param>
        public Armor(string name, int equipItemLevelReq, Slots itemSlot, PrimaryAttributes primaryAttribute, Armors type) : base(name, equipItemLevelReq, itemSlot)
        {
            PrimaryAttribute = primaryAttribute;
            Type = type;
            Name = name;
            EquipItemLevelReq = equipItemLevelReq;
            ItemSlot = itemSlot;
        }

        public Armors Type { get; set; }
        public PrimaryAttributes PrimaryAttribute { get; set; }
    
        /// <summary>
        /// Enum used for different types of armors.
        /// </summary>
        public enum Armors
        {
            Cloth,
            Leather,
            Mail,
            Plate,
        }
    }

}
