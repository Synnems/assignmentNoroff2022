﻿using Assignment1.Attributes;

namespace Assignment1.Items
{
    /// <summary>
    /// The Item base class, which have the the items Armor and Weapon as its children.
    /// </summary>
    public class Item
    {
        public string Name { get; set; }
        public int EquipItemLevelReq { get; set; }
        public Slots ItemSlot { get; set; } 
        public PrimaryAttributes P { get; set; }

        /// <summary>
        /// Item constructor which initialize Item with name, itemslot and item level requirement.
        /// </summary>
        /// <param name="name">Item name</param>
        /// <param name="equipItemLevelReq">Item equipped level requirement</param>
        /// <param name="itemSlot">What itemslot the item belongs in</param>
        public Item(string name, int equipItemLevelReq, Slots itemSlot)
        {
            Name = name;
            EquipItemLevelReq = equipItemLevelReq;
            ItemSlot = itemSlot;
        }
    }
}
