﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    /// <summary>
    /// Enum for the different type of item slots.
    /// </summary>
    public enum Slots
    {
        Weapon,
        Body,
        Legs,
        Head,
    }
}
