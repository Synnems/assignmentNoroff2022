﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.Items
{
    /// <summary>
    /// Weapon is a child class inheriting from the base class Item.
    /// </summary>
    public class Weapon : Item
    {
        /// <summary>
        /// Weapon constructor used to initalize weapons.
        /// </summary>
        /// <param name="name">Weapon name</param>
        /// <param name="equipItemLevelReq">What hero level is required to equip armor.</param>
        /// <param name="itemSlot">What type of item slot the weapon belongs in.</param>
        /// <param name="type">What type of weapon.</param>
        /// <param name="damage">How much damage the weapon deals.</param>
        /// <param name="attackSpeed">How fast attack speed on the weapon.</param>
        public Weapon(string name, int equipItemLevelReq, Slots itemSlot, Weapons type, double damage, double attackSpeed) : base(name, equipItemLevelReq, itemSlot)
        {
            Name = name;
            EquipItemLevelReq = equipItemLevelReq;
            Slots = itemSlot;
            Type = type;
            Damage = damage;
            AttackSpeed = attackSpeed;
        }

        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
        public double DamagePerSecond { get; set; }
        public Weapons Type { get; set; }
        public Slots Slots { get; set; }
        
        /// <summary>
        /// Calculate the weapons damage per second.
        /// </summary>
        /// <returns>A double value damage per second.</returns>
        public double GetDamagePerSecond()
        {
            return Damage * AttackSpeed;
        }

        /// <summary>
        /// Enum with weapon types
        /// </summary>
        public enum Weapons
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand,
        }
    }



}
