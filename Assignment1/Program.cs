﻿using Assignment1;
using Assignment1.Attributes;
using Assignment1.Heroes;
using Assignment1.Items;
using static Assignment1.Items.Armor;
using static Assignment1.Items.Weapon;

//Initializing new heroes.
Warrior Warrior = new Warrior("Ole");
Mage Mage = new Mage("Per");

//Making some test values.
PrimaryAttributes p = new PrimaryAttributes(100, 1, 2);
Weapon wand = new Weapon("wand", 1, Assignment1.Slots.Weapon, Weapons.Wand, 3.0, 1.1);
Weapon testAxe = new Weapon("Common axe", 1, Slots.Weapon, Weapons.Axe, 7, 1.1);
Armor testPlateBody = new Armor("Common plate body armor", 1, Slots.Body, new PrimaryAttributes(1, 0, 0), Armors.Plate);

//Equipping and leveling up Warrior "Ole".
Warrior.WeaponEquip(testAxe);
Warrior.ArmorEquip(testPlateBody, Slots.Body);
Warrior.LevelUp();

//Equipping and leveling up Mage "Per".
Mage.WeaponEquip(wand);
Mage.LevelUp();

//Display stats in console.
Warrior.StatsDisplay();
Mage.StatsDisplay();




