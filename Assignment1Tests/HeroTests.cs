using Assignment1.Heroes;

namespace Assignment1Tests
{
    public class HeroTests
    {
        [Theory]
        [InlineData(HeroName.Mage)]
        [InlineData(HeroName.Ranger)]
        [InlineData(HeroName.Warrior)]
        [InlineData(HeroName.Rogue)]
        public void HeroConstructor_CreateHero_HeroShouldBeLevel1WhenCreated(HeroName heroName)
        {
            //Arrange
            int levelWhenCreatedWanted = 1;
            Hero hero = CreateHero.createHeroes(heroName);

            //Act
            int levelWhenCreatedActual = hero.Level;

            //Assert
            Assert.Equal(levelWhenCreatedWanted, levelWhenCreatedActual);
        }

        [Theory]
        [InlineData(HeroName.Mage)]
        [InlineData(HeroName.Ranger)]
        [InlineData(HeroName.Warrior)]
        [InlineData(HeroName.Rogue)]
        public void LevelUp_HeroGainingLevel_HeroShouldBeLevel2(HeroName heroName)
        {
            //Arrange
            int levelWanted = 2;
            Hero hero = CreateHero.createHeroes(heroName);
            hero.LevelUp();
            
            //Act
            int levelWhenLeveledUp = hero.Level;

            //Assert
            Assert.Equal(levelWanted, levelWhenLeveledUp);
        }

        [Fact]
        public void BasePrimaryAttributes_MageCorrectAttributesWhenCreated_Str1Dex1Int8()
        {
            //Arrange
            int strengthWanted = 1;
            int dexterityWanted = 1;
            int intelligenceWanted = 8;
            Mage mage = new Mage("Per");

            //Act
            int strength = mage.PrimaryAttributes.Strength;
            int dexterity = mage.PrimaryAttributes.Dexterity;
            int intelligence = mage.PrimaryAttributes.Intelligence;

            //Assert
            Assert.Equal(strengthWanted, strength);
            Assert.Equal(dexterityWanted, dexterity);
            Assert.Equal(intelligenceWanted, intelligence);
        }

        [Fact]
        public void BasePrimaryAttributes_RangerCorrectAttributesWhenCreated_Str1Dex7Int1()
        {
            //Arrange
            int strengthWanted = 1;
            int dexterityWanted = 7;
            int intelligenceWanted = 1;
            Ranger ranger = new Ranger("Per");

            //Act
            int strength = ranger.PrimaryAttributes.Strength;
            int dexterity = ranger.PrimaryAttributes.Dexterity;
            int intelligence = ranger.PrimaryAttributes.Intelligence;

            //Assert
            Assert.Equal(strengthWanted, strength);
            Assert.Equal(dexterityWanted, dexterity);
            Assert.Equal(intelligenceWanted, intelligence);
        }

        [Fact]
        public void BasePrimaryAttributes_RogueCorrectAttributesWhenCreated_Str2Dex6Int1()
        {
            //Arrange
            int strengthWanted = 2;
            int dexterityWanted = 6;
            int intelligenceWanted = 1;
            Rogue rogue = new Rogue("Per");

            //Act
            int strength = rogue.PrimaryAttributes.Strength;
            int dexterity = rogue.PrimaryAttributes.Dexterity;
            int intelligence = rogue.PrimaryAttributes.Intelligence;

            //Assert
            Assert.Equal(strengthWanted, strength);
            Assert.Equal(dexterityWanted, dexterity);
            Assert.Equal(intelligenceWanted, intelligence);
        }

        [Fact]
        public void BasePrimaryAttributes_WarriorCorrectAttributesWhenCreated_Str5Dex2Int1()
        {
            //Arrange
            int strengthWanted = 5;
            int dexterityWanted = 2;
            int intelligenceWanted = 1;
            Warrior warrior = new Warrior("Ole");

            //Act
            int strength = warrior.PrimaryAttributes.Strength;
            int dexterity = warrior.PrimaryAttributes.Dexterity;
            int intelligence = warrior.PrimaryAttributes.Intelligence;

            //Assert
            Assert.Equal(strengthWanted, strength);
            Assert.Equal(dexterityWanted, dexterity);
            Assert.Equal(intelligenceWanted, intelligence);
        }

        [Fact]
        public void PrimaryAttributes_MageCorrectAttributesWhenLevelUp_Str2Dex2Int13()
        {
            //Arrange
            int strengthWanted = 2;
            int dexterityWanted = 2;
            int intelligenceWanted = 13;
            Mage mage = new Mage("Per");
            mage.LevelUp();

            //Act
            int strength = mage.PrimaryAttributes.Strength;
            int dexterity = mage.PrimaryAttributes.Dexterity;
            int intelligence = mage.PrimaryAttributes.Intelligence;

            //Assert
            Assert.Equal(strengthWanted, strength);
            Assert.Equal(dexterityWanted, dexterity);
            Assert.Equal(intelligenceWanted, intelligence);
        }

        [Fact]
        public void PrimaryAttributes_RangerCorrectAttributesWhenLevelUp_Str2Dex12Int2()
        {
            //Arrange
            int strengthWanted = 2;
            int dexterityWanted = 12;
            int intelligenceWanted = 2;
            Ranger ranger = new Ranger("Ole");
            ranger.LevelUp();

            //Act
            int strength = ranger.PrimaryAttributes.Strength;
            int dexterity = ranger.PrimaryAttributes.Dexterity;
            int intelligence = ranger.PrimaryAttributes.Intelligence;

            //Assert
            Assert.Equal(strengthWanted, strength);
            Assert.Equal(dexterityWanted, dexterity);
            Assert.Equal(intelligenceWanted, intelligence);
        }

        [Fact]
        public void PrimaryAttributes_RogueCorrectAttributesWhenLevelUp_Str3Dex10Int2()
        {
            //Arrange
            int strengthWanted = 3;
            int dexterityWanted = 10;
            int intelligenceWanted = 2;
            Rogue rogue = new Rogue("Per");
            rogue.LevelUp();

            //Act
            int strength = rogue.PrimaryAttributes.Strength;
            int dexterity = rogue.PrimaryAttributes.Dexterity;
            int intelligence = rogue.PrimaryAttributes.Intelligence;

            //Assert
            Assert.Equal(strengthWanted, strength);
            Assert.Equal(dexterityWanted, dexterity);
            Assert.Equal(intelligenceWanted, intelligence);
        }

        [Fact]
        public void PrimaryAttributes_WarriorCorrectAttributesWhenLevelUp_Str8Dex4Int2()
        {
            //Arrange
            int strengthWanted = 8;
            int dexterityWanted = 4;
            int intelligenceWanted = 2;
            Warrior warrior = new Warrior("Ole");
            warrior.LevelUp();

            //Act
            int strength = warrior.PrimaryAttributes.Strength;
            int dexterity = warrior.PrimaryAttributes.Dexterity;
            int intelligence = warrior.PrimaryAttributes.Intelligence;

            //Assert
            Assert.Equal(strengthWanted, strength);
            Assert.Equal(dexterityWanted, dexterity);
            Assert.Equal(intelligenceWanted, intelligence);
        }
    }
}