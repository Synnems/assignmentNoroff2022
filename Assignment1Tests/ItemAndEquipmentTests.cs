﻿using Assignment1.Items;
using Assignment1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Assignment1.Items.Weapon;
using Assignment1.Heroes;
using Assignment1.Exceptions;
using Assignment1.Attributes;

namespace Assignment1Tests
{
    public class ItemAndEquipmentTests
    {
        //Test values.
        Weapon testAxe = new Weapon("Common axe", 1, Slots.Weapon, Weapons.Axe, 7, 1.1);
        Weapon testBow = new Weapon("Common bow", 1, Slots.Weapon, Weapons.Bow, 12, 0.8);
        Armor testPlateBody = new Armor("Common plate body armor", 1, Slots.Body, new PrimaryAttributes(1, 0, 0), Armor.Armors.Plate);
        Armor testClothHead = new Armor("Common cloth head armor", 1, Slots.Head, new PrimaryAttributes(0,0,5), Armor.Armors.Cloth);

        [Fact]
        public void WeaponEquip_EquippingHighLevelWeapon_ThrowInvalidWeaponException() 
        {
            //Arange
            Warrior w = new Warrior("Ole");
            testAxe.EquipItemLevelReq = 2;

            //Act
            Action testWarrior = () => w.WeaponEquip(testAxe);

            //Assert
            Assert.Throws<InvalidWeaponException>(testWarrior);
        }

        [Fact]
        public void ArmorEquip_EquippingHighLevelArmor_ThrowInvalidArmorExeption() 
        {
            //Arrange
            Warrior w = new Warrior("Ole");
            testPlateBody.EquipItemLevelReq = 2;

            //Act
            Action testWarrior = () => w.ArmorEquip(testPlateBody, Slots.Body);

            //Assert
            Assert.Throws<InvalidArmorException>(testWarrior);
        }

        [Fact]
        public void WeaponEquip_EquipWrongWeaponType_ThrowInvalidWeaponException() 
        {
            //Arrange
            Warrior w = new Warrior("Ole");

            //Act
            Action testWarrior = () => w.WeaponEquip(testBow);

            //Assert
            Assert.Throws<InvalidWeaponException>(testWarrior);
        }

        [Fact]
        public void ArmorEquip_EquipWrongArmorType_ThrowInvalidArmorException() 
        {
            //Arrange
            Warrior w = new Warrior("Ole");

            //Act
            Action testWarrior = () => w.ArmorEquip(testClothHead, Slots.Head);

            //Assert
            Assert.Throws<InvalidArmorException>(testWarrior);
        }

        [Fact]
        public void WeaponEquip_EquipValidWeapon_ReturnSuccessMessage()
        {
            //Arrange
            Warrior w = new Warrior("Ole");

            //Act
            string result = w.WeaponEquip(testAxe);

            //Assert
            Assert.Equal("New weapon equipped!", result);
        }

        [Fact]
        public void ArmorEquip_EquipValidArmor_ReturnSuccessMessage() 
        {
            //Arrange
            Warrior w = new Warrior("Ole");

            //Act
            string result = w.ArmorEquip(testPlateBody, Slots.Body);

            //Assert
            Assert.Equal("New armour equipped!", result);
        }

        [Fact]
        public void CalculateDamage_CalculateDamageWithNoWeapon_ExpectedDamage()
        {
            //Arrange
            Warrior w = new Warrior("Ole");
            double wanted = 1.0 * (1.0 + (5.0 / 100.0));

            //Act
            double result = w.CalcDamage();

            //Result
            Assert.Equal(wanted, result);
        }

        [Fact]
        public void CalculateDamage_CalculateDamageWithWeponEquipped_ExpectedDamage()
        {
            //Arrange
            Warrior w = new Warrior("Ole");
            double wanted = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));
            w.WeaponEquip(testAxe);

            //Act
            double result = w.CalcDamage();

            //Assert
            Assert.Equal(wanted, result);
        }

        [Fact]
        public void CalculateDamage_CalculateDamageWithWeaponAndArmorEquipped_ExpectedDamage()
        {
            //Arrange
            Warrior w = new Warrior("Ole");
            double wanted = (7.0 * 1.1) * (1 + ((5.0 + 1.0) / 100.0));
            w.WeaponEquip(testAxe);
            w.ArmorEquip(testPlateBody, Slots.Body);

            //Act
            double result = w.CalcDamage();

            //Assert
            Assert.Equal(wanted, result);
        }
    }
}
